<?php 
class UserController extends BaseController
{
public function index()
	{
		return view('user/userPage');
	}
	public function upload()
	{
		if ($image = $this->request->getFile('photo')) { // validation success
			$input = $this->validate([
				'photo' => 'uploaded[photo]|max_size[photo,2024]|ext_in[photo,jpg,jpeg,png],'
			]);
			if ($input == 'true') {
				// file upload here
				if ($image->getSize() > 0 && $image->isValid()) {
					//echo $image->getName(); 	    echo "<br";
					// echo $image->getRandomName();   echo "<br";
					// echo $image->getSize();			echo "<br";
					// echo $image->getExtension();	echo "<br";	

					$image->move('./public/assets/img', $image->getRandomName());
					return redirect()->to('/UserController/index');
				}
			} else {
				//file upload error
				$data['validation'] = $this->validator;
				return view('user/userPage', $data);
			}
		}
		//return view('user/userPage');
	}
}
?>




<!-- view -->

<div class="form-group">
    <div class="col-md-6 mb-3">
      <label>Select Image : </label>
      <form method="post" enctype="multipart/form-data" action="<?php echo base_url('UserController/upload'); ?>">
        <input class="file" id="photo" name="photo" type="file" class="form-control"><br><br>

        <!-- Error -->
        <?php
        if (isset($validation) && $validation->hasError('photo')) { ?>
          <div class='alert alert-danger mt-2'>
            <?php echo $validation->getError('photo'); ?>
          </div>
        <?php   } ?>
        <input type="submit" name="upload" value="upload">
      </form>
    </div>
  </div>