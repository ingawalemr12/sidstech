<?= $this->extend('index') ?>

<?= $this->section('userPage') ?>

   <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <h2>Home Page </h2>
          <ol><li><a href="<?php echo base_url()?>/UserController">Home</a></li> </ol>
        </div>
      </div>
    </section>

    <section class="inner-page pt-4">
      <div class="container">
        <div class="section-header">
          <h2>Home </h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div>
      </div>
    </section>

  <br><br>

  <?= $this->endSection() ?>